from odoo import models, fields, api, exceptions
from odoo.tools.float_utils import float_compare

class EditorialLiquidacion(models.Model):
    """ Modelo de liquidación que extiende de account.move"""
    # https://github.com/OCA/OCB/blob/13.0/addons/account/models/account_move.py
    _description = "Liquidacion Descontrol"
    _inherit = ['account.move']

    @api.model
    def _get_default_is_liquidacion(self):
        if self._context.get('invoice_type') and self._context['invoice_type'] == 'LIQ':
            return True
        else:
            return False

    is_liquidacion = fields.Boolean('Es liquidacion', default=_get_default_is_liquidacion)

    def checkLiquidacionIsValid(self, liquidacion):
        products_to_check = []
        domain = [
            ('owner_id', '=', liquidacion.partner_id.id),
            ('location_id', '=' , 17), # This is the id of the Location DS/Depósitos (the source of the move)
            ('location_dest_id', '=' , 5), # This is the id of the Location Partner Locations/Customers (the destination of the move)
            ('state', 'in', ('assigned', 'partially_available')),
        ]
        pendientes_liquidar_line_ids = self.env['stock.move.line'].search(domain, order='date asc')
        for invoice_line in liquidacion.invoice_line_ids:
            invoice_line_qty = invoice_line.quantity
            pendientes_cerrar_stock_lines = pendientes_liquidar_line_ids.filtered(lambda pendientes_liquidar_line: pendientes_liquidar_line.product_id == invoice_line.product_id)
            if not pendientes_cerrar_stock_lines or len(pendientes_cerrar_stock_lines) <= 0:
                products_to_check.append((invoice_line.name, 0))
            else:
                sum_available = sum(p.product_uom_qty - p.qty_done for p in pendientes_cerrar_stock_lines)
                if sum_available < invoice_line_qty:
                    products_to_check.append((invoice_line.name, sum_available))
        return products_to_check

    def post_y_liquidar(self):
        if not self.is_liquidacion:
            raise exceptions.ValidationError("Sólo se puede liquidar desde una factura tipo liquidación")
        products_to_check = self.checkLiquidacionIsValid(self)
        if len(products_to_check) > 0:
            msg = "No hay stock suficiente disponible en depósito con estos valores. Estos son valores disponibles en depósito:"
            for product in products_to_check:
                msg += "\n* " + str(product[0]) + ": " + str(product[1])
            raise exceptions.UserError(msg)
        domain = [
            ('owner_id', '=', self.partner_id.id),
            ('location_id', '=' , 17), # This is the id of the Location DS/Depósitos (the source of the move)
            ('location_dest_id', '=' , 5), # This is the id of the Location Partner Locations/Customers (the destination of the move)
            ('state', 'in', ('assigned', 'partially_available')),
        ]
        pendientes_liquidar_line_ids = self.env['stock.move.line'].search(domain, order='date asc') # deposito de este contacto
        pickings_to_validate = set()
        for invoice_line in self.invoice_line_ids:
            invoice_line_qty = invoice_line.quantity
            pendientes_cerrar_stock_lines = pendientes_liquidar_line_ids.filtered(lambda pendientes_liquidar_line: pendientes_liquidar_line.product_id == invoice_line.product_id)
            if not pendientes_cerrar_stock_lines or len(pendientes_cerrar_stock_lines) <= 0:
                raise exceptions.ValidationError("No hay stock suficiente disponible en depósito con estos valores a liquidar. Intenta volver a comprobar el depósito")
            for pendiente_stock_line in pendientes_cerrar_stock_lines:
                if invoice_line_qty > 0:
                    stock_line_reserved_qty = pendiente_stock_line.product_uom_qty
                    stock_line_done_qty = pendiente_stock_line.qty_done
                    qty_difference = invoice_line_qty - (stock_line_reserved_qty - stock_line_done_qty)
                    if qty_difference >= 0: # if the invoice_qty is greater (or equal) than the available stock_qty, we close the stock_line and continue
                        pendiente_stock_line.write({'qty_done': stock_line_reserved_qty})
                    else:
                        pendiente_stock_line.write({'qty_done': stock_line_done_qty + invoice_line_qty})
                    picking = pendiente_stock_line.picking_id
                    pickings_to_validate.add(picking)
                    invoice_line_qty = qty_difference
                else:
                    break
        # Here we validate all the pickings at once:
        for picking in pickings_to_validate:
            picking.button_validate()
            ## Create backorder if necessary, because the button_validate() method doesn't do it for us automaticly
            if picking._check_backorder():
                moves_to_log = {}
                for move in picking.move_lines:
                    if float_compare(move.product_uom_qty, move.quantity_done, precision_rounding=move.product_uom.rounding) > 0:
                        moves_to_log[move] = (move.quantity_done, move.product_uom_qty)
                picking._log_less_quantities_than_expected(moves_to_log)
                picking.with_context(cancel_backorder=False).action_done()
        self.action_post()

class EditorialAccountMoveLine(models.Model):
    """ Extend account.move.line template for editorial management """
    _description = "Editorial Account Move Line"
    _inherit = 'account.move.line' # odoo/addons/account/models/account_move.py

    product_barcode = fields.Char(string='Código de barras / ISBN', related='product_id.barcode', readonly=True)
